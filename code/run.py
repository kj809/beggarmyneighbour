import pandas as pd

from code.BMN import *

all_games_data = []
names = ['Corrine', 'Kyle']

for _ in range(10000):
    game = Game()
    log = Log(game)
    Player.players = []
    [Player(name, game, deck, False) for name, deck in zip(names, Deck.split(Deck.shuffled()))]

    penalty = None
    while not game.game_ended():
        for player in Player.players:
            penalty = player.move(penalty)
    all_games_data.append(dict(log.counter))

df = pd.DataFrame.from_dict(all_games_data)
df.describe()