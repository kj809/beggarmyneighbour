import itertools
import random
import collections

class Deck:
    suits = ['H', 'D', 'S', 'C']
    ranks = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2']

    @classmethod
    def shuffled(cls):
        """return a shuffled deck"""
        Card = collections.namedtuple('Card', ['rank', 'suit'])
        cards = [Card(*card) for card in itertools.product(cls.ranks, cls.suits)]
        random.shuffle(cards)
        return cards

    @staticmethod
    def split(cards):
        """ split a list of cards into 2 equal lists"""
        num = len(cards)
        hand_size = int(num / 2)
        return cards[:hand_size], cards[hand_size:]


class Game:
    penalties = {'J': 1, 'Q': 2, 'K': 3, 'A': 4}

    def __init__(self):
        self.game_deck = collections.deque()

    def return_penalty(self):
        """ check state of game deck to decide penalty to apply"""
        top_card = self.game_deck[-1]
        penalty = self.penalties.get(top_card.rank)
        return penalty

    def game_ended(self):
        """ check if the game is finished or not"""
        for player in Player.players:
            if len(player.hand) in (0, 52):
                return True
        return False


class Player:
    players = []

    def __init__(self, name, game, cards, verbose=False):
        self.name = name
        self.game = game
        self.log = game.log
        self.hand = collections.deque(cards)
        self.verbose = verbose
        Player.players.append(self)

    def __repr__(self):
        return str(self.name)

    def top_n_cards(self, n):
        return list(self.hand)[-n:]

    def move(self, penalty):
        """ place a card into the game_deck, given penalty """

        if self.game.game_ended():
            """ first check the game is not over and that we can make a valid move """
            return None

        self.log.counter['turns'] += 1

        if penalty:
            """ if a penalty applies from prior player, then iterate through until
                we encounter a penalty card.
                If we finish our own penalty, return "collect deck"   """
            if penalty == "collect deck":
                """ if a value of "collect deck" is returned from opponent, then we pickup da deck """
                self.hand.extendleft(list(reversed(self.game.game_deck)))
                self.game.game_deck = collections.deque()
                self.log.counter['pickups'] += 1
                if self.verbose:
                    print("{:10} collects deck".format(self.name))
            else:
                """ if "collect deck" is not returned from opponent, the penalty value corresponds
                    to the number of cards we need to place down """
                self.log.counter['penalties'] += 1
                for _ in range(penalty):
                    if self.game.game_ended():
                        return None
                    card = self.hand.pop()
                    self.game.game_deck.append(card)
                    self.log.counter['cards played'] += 1
                    new_penalty = self.game.return_penalty()
                    if self.verbose:
                        print("{:10} plays {}, hand sizes: {}".format(self.name,
                                                                      card,
                                                                      [len(p.hand) for p in Player.players]))
                    if new_penalty:
                        return new_penalty
                return "collect deck"

        """ if no penalty applies, or if we have already collected the deck, play a card """
        card = self.hand.pop()
        self.game.game_deck.append(card)
        self.log.counter['cards played'] += 1

        if self.verbose:
            print("{:10} plays {}, hand sizes: {}".format(self.name,
                                                          card,
                                                          [len(p.hand) for p in Player.players]))
        return self.game.return_penalty()


class Log:
    ##what the hell is this, like seriously wtf is the point of this class shiiiiiiiiii

    """log stats about a game here"""
    def __init__(self, game):
        self.counter = collections.Counter()
        game.log = self