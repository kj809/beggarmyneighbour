FROM python:3.6

COPY ./requirements.txt /requirements.txt

RUN pip install -r requirements.txt

COPY ./code /code

WORKDIR /code

RUN python run.py

#install "docker built -t (name) ."
#run "docker run -it (name)" (see output)